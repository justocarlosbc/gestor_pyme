from django.contrib.auth.decorators import login_required
from django.urls import path
from Core.erp.views.categoria.views import *
from Core.erp.views.dashboard.views import *
from Core.erp.views.product.views import *

urlpatterns = [
    path('categoria/list/', login_required(CategoriaListView.as_view()), name='category_list'),
    path('categoria/add/', login_required(CategoryCreateView.as_view()), name='category_create'),
    path('categoria/update/<int:pk>', login_required(CategoryUpdateView.as_view()), name='category_update'),
    path('categoria/delete/<int:pk>', login_required(CategoryDeleteView.as_view()), name='category_delete'),

    # dashboard
    path('dashboard/', login_required(DashboardView.as_view()), name='dashboard'),

    # product
    path('productos/list/', login_required(ProductListView.as_view()), name='product_list'),
    path('productos/add/', login_required(ProductCreateView.as_view()), name='product_create'),
    path('productos/update/<int:pk>/', login_required(ProductUpdateView.as_view()), name='product_update'),
    path('productos/delete/<int:pk>/', login_required(ProductDeleteView.as_view()), name='product_delete'),
]
