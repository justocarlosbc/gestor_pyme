from django.apps import AppConfig


class LoginConfig(AppConfig):
    name = 'Core.login'
